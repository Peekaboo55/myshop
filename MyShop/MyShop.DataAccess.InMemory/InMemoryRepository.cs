﻿using MyShop.Core.Contracts;
using MyShop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace MyShop.DataAccess.InMemory
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        ObjectCache cache = MemoryCache.Default;
        List<T> item;
        string className;

        public InMemoryRepository()
        {
            className = typeof(T).Name;
            item = cache[className] as List<T>;
            if (item == null)
                item = new List<T>();
        }

        public void Commit()
        {
            cache[className] = item;
        }

        public void Insert(T t)
        {
            item.Add(t);
        }

        public void Update(T t)
        {
            T tToUpdate = item.Find(i => i.Id == t.Id);

            if (tToUpdate != null)
            {
                tToUpdate = t;
            }
            else
            {
                throw new Exception(className + " Not found");
            }
        }

        public T Find(string Id)
        {
            T t = item.Find(i => i.Id == Id);
            if (t != null)
            {
                return t;
            }
            else
            {
                throw new Exception(className + " Not found");
            }
        }

        public IQueryable<T> Collection()
        {
            return item.AsQueryable();
        }

        public void Delete(string Id)
        {
            T tToDelete = item.Find(i => i.Id == Id);

            if (tToDelete != null)
            {
                item.Remove(tToDelete);
            }
            else
            {
                throw new Exception(className + " Not found");
            }
        }
    }
}
